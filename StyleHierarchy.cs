﻿

namespace NoniB.Ecom.Model
{
    public class StyleHierarchy
    {
         public string Code { get; set; }

         public string Label { get; set; }

         public string ShortLabel { get; set; }

         public string HierarchyLevel { get; set; }

         public string ParentCode { get; set; }

         public bool ActiveFlag { get; set; }
    }
}
