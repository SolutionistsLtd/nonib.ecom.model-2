﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoniB.Ecom.Model
{
    public class Inventory
    { 
        public String LocationCode { get; set; }
        
        public String SkuId { get; set; }
        
        public int SohUnits { get; set; }

        public int TotalSohUnits { get; set; }
    }

    ///// <summary>
    ///// The object to pass to the /locationsoh resouce of the API.
    ///// </summary>
    //public class SOHSkuDetail
    //{
    //    public double Latitude { get; set; }
    //    public double Longitude { get; set; }
    //    public decimal Distance { get; set; }
    //    public List<string> SkuIds { get; set; }
    //}
}
