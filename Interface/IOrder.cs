﻿using System;
using System.Collections.Generic;

namespace NoniB.Ecom.Model
{
    public interface IOrder
    {
        List<OrderAddress> Addresses { get; set; }
        BrandCode BrandCode { get; set; }
        DateTime? CreateDate { get; set; }
        Customer Customer { get; set; }
        decimal DeliveryDiscountAmount { get; set; }
        string DeliveryInstructions { get; set; }
        List<OrderItem> Items { get; set; }
        DateTime? LastUpdatedDate { get; set; }
        string LocationCode { get; set; }
        Channel OrderChannel { get; set; }
        long OrderId { get; set; }
        string OrderNumber { get; set; }
        string OrderReference { get; set; }
        OrderType OrderType { get; set; }
        string OriginalOrderNumber { get; set; }
        List<OrderPayment> Payments { get; set; }
        string ShipmentType { get; set; }
        decimal ShippingAmount { get; set; }
        decimal ShippingTax { get; set; }
        decimal TotalOrderRetail { get; set; }
        decimal TotalOrderTax { get; set; }
        List<OrderVoucher> Vouchers { get; set; }
    }
}