﻿using System.Collections.Generic;

namespace NoniB.Ecom.Model
{
    public interface IOrderItem
    {
        List<string> CouponCode { get; set; }
        List<string> DiscountReason { get; set; }
        string FulfillmentStore { get; set; }
        decimal LineDiscount { get; set; }
        string OrderItemStatus { get; set; }
        string OrderLineId { get; set; }
        int QuantityOrdered { get; set; }
        string ReturnReason { get; set; }
        string SkuId { get; set; }
        decimal UnitRetail { get; set; }
        decimal UnitTax { get; set; }
    }
}