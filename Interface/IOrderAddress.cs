﻿namespace NoniB.Ecom.Model
{
    public interface IOrderAddress
    {
        AddressType AddressType { get; set; }
        string AttentionOf { get; set; }
        string City { get; set; }
        string Country { get; set; }
        string Email { get; set; }
        string FirstName { get; set; }
        string Phone { get; set; }
        string Postcode { get; set; }
        string State { get; set; }
        string StreetAddress1 { get; set; }
        string StreetAddress2 { get; set; }
        string Suburb { get; set; }
        string Surname { get; set; }
    }
}