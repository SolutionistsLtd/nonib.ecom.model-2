﻿using System;
using System.Collections.Generic;

namespace NoniB.Ecom.Model
{
    public interface ICustomer
    {
        List<CustomerAddress> Addresses { get; set; }
        List<CustomerBrandFlag> BrandFlags { get; set; }
        string CustomerNumber { get; set; }
        InstanceStatus DataStatus { get; set; }
        DateTime? DateOfBirth { get; set; }
        string EmailAddress { get; set; }
        string FirstName { get; set; }
        string Mobile { get; set; }
        string Phone { get; set; }
        List<PromotionalVoucher> PromotionalVouchers { get; set; }
        decimal RewardBalance { get; set; }
        string RewardProgram { get; set; }
        string Source { get; set; }
        string Surname { get; set; }
        string Title { get; set; }
    }
}