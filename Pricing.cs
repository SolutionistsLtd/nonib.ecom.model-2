﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoniB.Ecom.Model
{
    public class Pricing
    {
        /// <summary>
        /// Epicor price change document number
        /// </summary>
        
        public string PriceChangeNumber { get; set; }

        /// <summary>
        /// Description in Epicor of the price change document
        /// </summary>
        
        public string PriceChangeName { get; set; }

        /// <summary>
        /// Effective from will be the date the new price is active on.  This needs to include time. An actual time.      
        /// </summary>        
        public DateTime EffectiveFrom { get; set; }

        /// <summary>
        /// Effective to will be null if it is a permanent price change        
        /// </summary>
        public DateTime? EffectiveTo { get; set; }

        /// <summary>
        /// Effective to will be null if it is a permanent price change
        /// </summary>
        public List<PricingSku> Skus { get; set; }
    }
}
