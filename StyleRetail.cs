﻿namespace NoniB.Ecom.Model
{
    public class StyleRetail
    {
        public string CurrencyCode { get; set; }
        public string PriceStatus { get; set; }
        public decimal OriginalRetailInc { get; set; }
        public decimal OriginalRetailExc { get; set; }
        public decimal CurrentRetailInc { get; set; }
        public decimal CurrentRetailExc { get; set; }
    }
}