﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoniB.Ecom.Model
{
    public class GiftCardCharge
    {
        public string GiftCardNumber { get; set; }
        public string GiftCardPin { get; set; }
       
        public decimal ChargeValue { get; set; }
        public BrandCode BrandCode { get; set; }
        public string OrderNumber { get; set; }

    }
}
