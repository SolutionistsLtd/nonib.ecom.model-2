﻿using System;
using System.Collections.Generic;

namespace NoniB.Ecom.Model
{
    public class Order : IOrder
    {

        /// <summary>
        /// NoniB Internal Order Id
        /// </summary>
        public long OrderId { get; set; }


        /// <summary>
        /// SiteName : Rockmans, BeMe, WLane, Millers, Katies, Rivers, Crossroads, Autograph
        /// </summary>
        public BrandCode BrandCode { get; set; }

        /// <summary>
        /// Customer on the order.  
        /// </summary>
        public Customer Customer { get; set; }

        /// <summary>
        /// Order Number from Ecommerce
        /// </summary>

        public string OrderNumber { get; set; }

        /// <summary>
        /// Customer facing Order Number
        /// </summary>

        public string OrderReference { get; set; }

        /// <summary>
        /// Used for returns and exchanges
        /// </summary>

        public string OriginalOrderNumber { get; set; }

        /// <summary>
        /// New(Sale), Refund, Exchange
        /// enum OrderType
        /// </summary>
        //[JsonConverter(typeof(StringEnumConverter))]
        public OrderType OrderType { get; set; }
        

        public Headertatus Status { get; set; }

        /// <summary>
        /// Website Location Code : e.g. 1301
        /// </summary>
        public string LocationCode { get; set; }

        /// <summary>
        /// enum OrderChannel
        /// </summary>
        public Channel OrderChannel { get; set; } // SOL will either pass Click&Collect or a NULL value bases on the order.

        /// <summary>
        /// Order create date.  When the order is originally created.  Should this be UTC?
        /// </summary>
        public DateTime? CreateDate { get; set; }
        /// <summary>
        /// Delivery instructions placed on shipping label for customer.
        /// </summary>

        public bool AuthorityToLeave { get; set; }
        public string DeliveryInstructions { get; set; }


        public decimal DeliveryDiscountAmount { get; set; }

        /// <summary>
        /// Junk at the moment. I propose we make this an Enum
        /// We're hard coding to "Next Day" when we sent to to Pronto. I think that wanted the eventually add 'Same Day' and 'Express'
        /// </summary>
        public string ShipmentType { get; set; }
        /// <summary>
        /// DeliveryAmount -> ShippingAmount
        /// </summary>
        public decimal ShippingAmount { get; set; }
        /// <summary>
        /// DeliveryTax -> ShippingTax
        /// </summary>
        public decimal ShippingTax { get; set; }
        /// <summary>
        /// DB: total_order_amount
        /// Order total
        /// </summary>
        public decimal TotalOrderRetail { get; set; }
        /// <summary>
        /// Order tax total
        /// </summary>
        public decimal TotalOrderTax { get; set; }

        public bool IsGuestCheckout { get; set; }

        public DateTime? LastUpdatedDate { get; set; }

        public List<OrderItem> Items { get; set; }

        public List<OrderVoucher> Vouchers { get; set; }

        public List<OrderPayment> Payments { get; set; }

        public List<OrderAddress> Addresses { get; set; }
    }



}
