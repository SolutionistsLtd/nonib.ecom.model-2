﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoniB.Ecom.Model
{
    public class LocationHierarchy
    {
         public string Code { get; set; }
         public string Label { get; set; }
         public string ShortLabel { get; set; }
         public string HierarchyLevel { get; set; }
         public string ParentCode { get; set; }
         public bool ActiveFlag { get; set; }
    }
}
